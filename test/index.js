/**
 * Test suite entrance point
 */

const path = require('path');
const chai = require('chai');
const proxyquire = require('proxyquire');

const expect = chai.expect;

const testProjectRoot = path.resolve(__dirname, 'test-project');
const testProject = require(testProjectRoot).default;

describe('config-bark', function () {

    it('should rock.', function () { return true; });

    describe('util', function () {

        const { walkUpTree, parseConfigFile } = require('../src/util');

        describe('walkUpTree', function () {

            it('should find the project root', function () {

                const startAt = path.resolve(testProjectRoot, 'deeply', 'nested');

                expect(walkUpTree(startAt)).to.equal(testProjectRoot);
            });

            it('should throw if it reaches the root directory', function () {

                const rootPath = path.parse(__dirname).root;

                const shouldThrow = () => { walkUpTree(path.resolve(rootPath, 'a', 'fake', 'path')); };

                expect(shouldThrow).to.throw();
            });

            it('should not throw if told not to', function () {

                const rootPath = path.parse(__dirname).root;

                const shouldNotThrow = () => walkUpTree(path.resolve(rootPath, 'a', 'fake', 'path'), false);

                expect(shouldNotThrow).to.not.throw();
                expect(shouldNotThrow()).to.equal(false);
            });
        });

        describe('parseConfigFile', function () {

            it('should parse the given config file', function () {

                expect(parseConfigFile(testProjectRoot, 'development').logLevel).to.equal('debug');
            });

            it('should throw by default if the given config file cannot be parsed', function () {

                const shouldThrow = () => { parseConfigFile(__dirname, 'development'); };

                expect(shouldThrow).to.throw();
            });

            it('should not throw if told not to', function () {

                const shouldNotThrow = () => { parseConfigFile(__dirname, 'development', false); };

                expect(shouldNotThrow).to.not.throw();
            });
        });
    });

    describe('autoloading config', function () {

        beforeEach(function () {

            delete process.env.NODE_ENV;
        });

        it('should automatically load config files if it can find them', function () {

            const config = proxyquire('../src/index', {
                './util': { walkUpTree: () => testProjectRoot }
            }).default;

            expect(config.logLevel('fallback')).to.equal('debug');
        });

        it('should not throw if it fails to autoload a config file', function () {

            process.env.NODE_ENV = 'test';

            const config = proxyquire('../src/index', {
                './util': { walkUpTree: () => testProjectRoot }
            }).default;

            expect(config.logLevel('fallback')).to.equal('fallback');
        });
    });

    describe('config access', function () {

        beforeEach(function () {

            delete process.env.NODE_ENV;
        });

        it('should expose a __load__ method on the root object', function () {

            const config = testProject();

            expect(config.__load__).to.be.a('function');
        });

        it('should return config values that exist', function () {

            const config = testProject();

            expect(config.deeply.nested()).to.equal('value');
        });

        it('should return the fallback for values that do not exist', function () {

            const config = testProject();

            expect(config.does.not.exist('fallback')).to.equal('fallback');
        });

        it('should load the requested config file', function () {

            const config = testProject('production');

            expect(config.logLevel()).to.equal('info');
        });

        it('should fallback to process.env.NODE_ENV if no env given', function () {

            process.env.NODE_ENV = 'production';

            const config = testProject();

            expect(config.logLevel()).to.equal('info');
        });

        it('should throw if the expected config file does not exist', function () {

            const shouldThrow = () => { testProject('test'); };

            expect(shouldThrow).to.throw();
        });

        it('should only require __load__ to be called once', function () {

            testProject();

            const second = require(path.resolve(testProjectRoot, 'second')).default;

            expect(second.logLevel()).to.equal('debug');
        });
    });
});
