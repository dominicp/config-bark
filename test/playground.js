const isPrimitive = require('is-primitive');

const dummyData = JSON.stringify({
    deeply: {
        nested: {
            prop: 'found me'
        }
    }
});

class UndefinedProp {}

class PrimitiveVal {

    constructor(value) {

        this.val = value;
    }

    get() { return this.val; }
}

// const isPrimitive = v => (v === null || Number.isNaN(v) || ! ['function', 'object'].includes(typeof v));

const getProxy = (data, allowLoad = false) => new Proxy(() => {}, {

    apply(target, thisArg, argList) {

        const defaultValue = argList[0];

        if (data instanceof UndefinedProp) { return defaultValue; }

        if (data instanceof PrimitiveVal) { return data.get(); }

        return data;
    },

    get(target, prop) {

        if (prop === '_load' && allowLoad) {

            return () => { data = JSON.parse(dummyData); }; // eslint-disable-line no-param-reassign
        }

        // Determine the new data to load at the next level
        let newData = new UndefinedProp();

        if (data && (prop in data) && !(data instanceof UndefinedProp)) {

            newData = data[prop];
        }

        if (isPrimitive(newData)) {

            newData = new PrimitiveVal(newData);
        }

        return getProxy(newData);
    }
});

module.exports = getProxy({}, true);
