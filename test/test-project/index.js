/**
 * An example project for testing config laoding functionality
 */
import config from '../../src/index';

export default (env) => {

    config.__load__(__dirname, env);

    return config;
};
