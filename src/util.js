/**
 * We keep our helper functions here for clarity and easier testing.
 */
import fs from 'fs';
import path from 'path';

/**
 * A simple helper function to walk up a directory tree looking for a directory that has a package.json
 * file in it. When we find one, we assume that to be the project root. We export it mainly for testing
 * purposes.
 *
 * @param  {String} currentDir The current directory to search in
 * @param  {Boolean} throwOnFailure Whether or not to throw on failure
 * @param  {Integer} callCount Since this is a recursive function we use this to prevent infinite loops.
 *
 * @return {String}            The path to the root directory or false on failure
 */
export const walkUpTree = (currentDir, throwOnFailure = true, callCount = 0) => {

    try {

        if (callCount > 50) {

            throw new Error('Too much recursion walking up tree to project root. Infinite loop?');
        }

        if (fs.existsSync(path.resolve(currentDir, 'node_modules'))
            && fs.existsSync(path.resolve(currentDir, 'package.json'))) {

            return currentDir;
        }

        const rootPath = path.parse(currentDir).root;

        if (currentDir === rootPath) {

            throw new Error('Reached root path and failed to find project root.');
        }

        return walkUpTree(path.dirname(currentDir), throwOnFailure, callCount + 1);

    } catch (error) {

        if (throwOnFailure) { throw error; }
        return false;
    }
};

/**
 * A simple helper to parse a config file given a base dir and an environment.
 *
 * @param  {String}  baseDir              The project root dir where config files live
 * @param  {String}  environment          The environment that defines the config file to parse
 * @param  {Boolean} [throwOnErrors=true] Whether or not we should throw on failures
 *
 * @return {Object}                       Either the parsed config file or null on failure
 */
export const parseConfigFile = (baseDir, environment, throwOnErrors = true) => {

    let configData = null;
    const fileName = (environment || 'development') + '.json';

    try {

        configData = JSON.parse(fs.readFileSync(path.resolve(baseDir, 'config', fileName)));

    } catch (error) {

        if (throwOnErrors) { throw error; }
    }

    return configData;
};
