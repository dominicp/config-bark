/**
 * Load the config file at the expected location and export it.
 */
import isPrimitive from 'is-primitive';
import { walkUpTree, parseConfigFile } from './util';

class UndefinedProp {}

class PrimitiveVal {

    constructor(value) {

        this.val = value;
    }

    get() { return this.val; }
}

const getProxy = (data, allowLoad = false) => new Proxy(() => {}, {

    apply(target, thisArg, argList) {

        const defaultValue = argList[0];

        if (data instanceof UndefinedProp) { return defaultValue; }

        if (data instanceof PrimitiveVal) { return data.get(); }

        return data;
    },

    get(target, prop) {

        // Special case to load the config data
        if (prop === '__load__' && allowLoad) {

            return (startingDir, environment = process.env.NODE_ENV) => {

                // Attempt to parse the config file (will throw on errors)
                data = parseConfigFile(walkUpTree(startingDir), environment); // eslint-disable-line no-param-reassign
            };
        }

        // Determine the new data to load at the next level
        let newData = new UndefinedProp();

        if (data && (prop in data) && !(data instanceof UndefinedProp)) {

            newData = data[prop];
        }

        if (isPrimitive(newData)) {

            newData = new PrimitiveVal(newData);
        }

        return getProxy(newData);
    }
});

const getConfig = () => parseConfigFile(walkUpTree(__dirname, false), process.env.NODE_ENV, false);

// Attempt to locate and preload config so we don't have to call __load__
export default getProxy(getConfig() || {}, true);
