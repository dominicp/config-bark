module.exports = {
    target: 'node',
    mode: 'development',
    devtool: 'source-map',
    output: {
        library: 'configBark',
        libraryTarget: 'umd'
    },
    optimization: {
        nodeEnv: false
    },
    node: {
        __dirname: false
    }
};
